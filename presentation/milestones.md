# Milestones 01.12.2020

- Research and understand Sancus

<table border="0">
 <tr>
    <td><b style="font-size:30px">Software-Sim</b></td>
    <td><b style="font-size:30px">FPGA</b></td>
 </tr>

 <tr>
    <td>

- Create simulation of Sancus nodes: \\\ max. 4 weeks
    - Create multiple dockerized Sancus Nodes 
    - Establish communication via http requests. 
      Nodes should be able to transfer information and 
      be able to authenticate the validity by using 
      the sancus library and the keys they were given at startup.
    

- Create Sancus Tutorials: 
    - Create an expressive turoial how to setup different sancus based 
      solutions (i.e. for software vendors and/or infastructure providers) \\\max. 2 weeks
    </td>


    <td>

* Set up connection to FPGA
- Set up remote toolchain to FPGA (Laptop with TeamViewer connected to FPGA + Web-Cam showing FPGA)
- Run example-files on FPGA
- Deploy OpenMSP to Cyclone V:
    - Reimplement and understand the work of the predecessor-project-group (swp_2019_sancus)
    - Fix open issues
- Deploy Sancus-examples to FPGA
    </td>
 </tr>
</table>

- Run own Sancus-examples on FPGA

# Minimum scenario:
- Create Docker-based software-sim
- Reimplement and understand the work of the predecessor-project-group

# Best-case scenario:
- Deploy OpenMsp on Cyclone V
- Deploy and demonstrate a sancus node with integrated websever on Cyclone V as previous mentioned 
    - test modules communicating with each other via the server
    - test possible security vulnerabilities and flaws
