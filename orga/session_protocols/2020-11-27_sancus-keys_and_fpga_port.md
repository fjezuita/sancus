# Sancus-Keys and FPGA-Port
- Basti, Fabian (27.11.2020)

## Sancus-Keys
- there are multiple keys used in the sancus-architecture
- K_N a fixed key etched into each node shared by all nodes of a system
- there is a function to generated further keys kdf. it takes 2 arguments: a key and some sort of ID
- the software provider (SP) has a unique ID
- therefore each SP has its own key: K_N_SP = kdf(K_N, SP_ID)
- also, each software module (SM) has an ID and can therefore generate its own key: K_N_SP_SM = kdf(K_N_SP, SM_ID)
- these keys are used to ensure secure communication between all instances on the various levels, respectively
- this is implemented by using a  MAC(message authentication code)-function that generates MAC-codes based on these keys

(Hardware-Based Trusted Computing
Architectures for Isolation and Attestation by
Pieter Maene, Johannes Götzfried and others, 5.6
Sancus)

## FPGA-Port
- the original sancus-project uses an older FPGA(DE1) with an extended OpenMSP430-architecture
- the swp2019 started porting this extended OpenMSP-desigin to the DE10-standard, but never finished it… :(
- the DE10 uses VHDL as hardware-description-language and Quartus to deploy it onto the FPGA