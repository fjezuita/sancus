# project progress

## 2020-12-1: group meeting (all)
- general coordination
- presentation (2020-12-8) preparation 

## 2020-12-11: meeting FPGA-sub-team (Sebastian,Fabian)
- succesfully compiled Sancus_OpenMSP430 for the DE10
- used code from https://github.com/AntonHermann/sancus-core/tree/master/fpga/altera_de10_standard
- followed the guide from SWP-2019: https://git.imp.fu-berlin.de/raup90/swp_2019/-/blob/master/docs/DE10-standard/run_project.md